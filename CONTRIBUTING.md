# The Good Docs Project Contributing Guide

Welcome to The Good Docs Project Contributing Guide, and thank you for your interest.
The purpose of the The Good Docs Project is to educate and empower people to create high-quality documentation by providing them with resources, best practices, and tools to enhance their documentation in open source and beyond.


## Join our project

If you would like to contribute to a specific part of the project, we ask that you attend a [Welcome Wagon meeting](https://thegooddocsproject.dev/welcome/).
At this 30-minute orientation meeting, you’ll get:

- A brief overview of our project’s goals and mission.
- A bit of information about our community and reasons to consider joining.
- An overview of our key initiatives and working groups that you might consider contributing to.
- A chance to tell us your goals for contributing as well as your current skill/experience levels to see if we can find a working group and a good first task for you.

To register for this meeting, we need you to fill out the [Welcome Wagon form](https://thegooddocsproject.dev/welcome/). Your privacy will be respected, and this information will only be shared with people in the project on a need-to-know basis.

Attending this meeting will give you access to community forums and allow you to request membership to any our repositories.


## How we work

Most of our work is done by participating in one of our working groups, which generally meet weekly or bi-weekly. Most community members give about 1-2 hours of their time every week by participating in one of these groups.

Always remember that we’ll take what you can give. You and your family come first, then work, then volunteering. If you can’t keep your working group commitments, that’s okay. Just let your working group leader know.



## Ground rules

Before contributing, read our [Code of Conduct](https://thegooddocsproject.dev/code-of-conduct/) to learn more about our community guidelines and expectations.