# Policies for Project Related Passwords

We make an effort to centeralize all project passwords under a single 'owner' account when possible.
Passwords for products and services associated with that account are managed with the following policies.

* Add Good Docs Google Account as the owner to all services
* Only Tech Team owners have access to the Google account
* If only one login is allowed for the account, only the Good Docs account has access and that password is stored in 1password
* If multiple logins are allowed (like additional users), the Good Docs account can create additional logins
* To request access to a service, open a request with the tech team
* We'll set up a password rotation calendar to rotate passwords once a year
