# The Good Docs Project Project Steering Committee

The Good Docs Project Project Steering Committee (PSC) is the official managing body of The Good Docs Project and is responsible for setting overall project direction. The committee is drawn from The Good Docs Project community and is based on merit and interest.

## PSC Members

See our [Who We Are](https://thegooddocsproject.dev/who-we-are/) page on our website for the list of current PSC members and list of responsibilities.

## Decisions

Decisions are based on voting using the following:

-   +1 : For
-   +0: Mildly for, but mostly indifferent
-   0: Indifferent
-   -0: Mildly against, but mostly indifferent
-   -1 : Strongly against. This is expected to be accompanied by a
    reason.

For most decisions, a motion is raised within one of TheGoodDocsProject open forums (slack, email list, etc) and those present vote and decide. Should a decision:

-   Be strategic or impact many within TheGoodDocsProject,
-   Or if there is contention in deciding, and after discussion at least one member is still strongly against the motion, by continuing to vote -1,

Then the motion should be referred to the PSC.

For motions presented to the PSC, a motion is considered passed when there are more PSC +1 votes than -1 votes, and either all PSC members have voted, or 4 days have passed. In the case of a tie, the PSC chair shall have 1.5 votes.

*Credit: This decision making process is derived from the [OSGeoLive](https://live.osgeo.org/) PSC process.*
