---
title: The Good Docs Project and the GitLab Open Source Partner program
---

## Prologue

During the 2023 renewal of the [GitLab Open Source Program](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/#gitlab-for-open-source-program) subscription for The Good Docs Project (TGDP), Bryan Klein (BK) and Alyssa Rock (AR) were contacted by [Bryan Behrenshausen](https://gitlab.com/bbehr) (BB) for some license compliance issues in TGDP repositories.
This conversation led to BB sharing information about the GitLab Open Source Partner program, along with an invitation for TGDP to consider becoming a partner.

This information was discussed in Tech Team meetings and brought to TGDP Project Steering Committee (PSC). 
Later BB joined a session of TGDP General Meeting to share some information about the program to those in attendance.

Many good [questions and concerns from TGDP PSC](#the-good-docs-project-perspective) have been raised, and further conversations for clarification took place.

## GitLab Open Source Partner program

From the Partner Program Guide:

>The GitLab Open Source Partners program exists to build relationships with prominent open-source projects using GitLab as a critical component of their infrastructure.  
>Through our partnership, we hope to strengthen the open-source ecosystem.

At a high level, the following Partner benefits are stated on the [GitLab Open Source Partners](https://about.gitlab.com/solutions/open-source/partners/) website.

- Public recognition as a GitLab Open Source Partner
- Direct line of communication to GitLab
- Assistance migrating additional infrastructure to GitLab
- Exclusive invitations to participate in GitLab events
- Opportunities to meet with and learn from other open source partners
- Visibility and promotion through GitLab marketing channels

The Requirements for Partners are:

- Engage in co-marketing efforts with GitLab
- Complete a public case study about their innovative use of GitLab
- Plan and participate in joint initiatives and events

More detail about the program can be found on the [Open Source Program Handbook](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/#gitlab-open-source-partners) page.

The [GitLab Open Source Partners Program Guide (PDF)](https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-open-source-partners/community-resources/-/blob/main/program-guide/gitlab-open-source-partners-program-guide.pdf).

There is also the [Partners Group](https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/gitlab-open-source-partners) on GitLab.

## The Good Docs Project Perspective

The response to this invitation has been largely positive from TGDP PSC.

> "I think this is a major milestone for our project. I'm especially impressed that GitLab wants to showcase our project to demonstrate that people who come from non-code backgrounds can make meaningful contributions to open source." (AR)

> "This is big step in the right direction for us and a great opportunity to engage with other open source projects." (CC)

> "It will be great to start engaging with other open source projects. A number of other Partners members have pretty thorough docs programs, and would make for great beta users." (AP)

> "As everybody mentioned, this is a huge step, and I am excited about this collaboration between GitLab and TGDP." (GK)

Some concerns have been noted for awareness as we move forward, to make sure that the partnership remains mutually beneficial for all parties involved.

The noted concerns are:

- Confirm that the Open Source licensing decisions made by TGDP for project repositories and content are based on what is best for the project and not dictated by the Partnership program. (RM)
- "Big fish-little fish" collaborative dynamics can go poorly. Be very eyes-open and intentional as we move forward. (RM)
- Verify that the 'offboarding' process is relatively easy and painless if we decide not to remain in the program. (CS)
- Need to know the specifics on requirements of TGDP to participate in the program so that effort and time required to sustain the partnership can be estimated. (GK)

## Relevant responses from Bryan Behrenshausen

Regarding the 'offboarding' process if TGDP decides to end the Partnership.

> "Ending the partnership will be as simple and immediate as emailing me to tell me you've decided to leave the community and withdraw your membership, at which point I'd remove your logo from the partners page and revoke any access privileges your community members might have (to partner groups and communication channels, etc.). But that's it—no long-term obligations, no lock-in, no intention of undermining your community's autonomy."

Regarding time and effort requirements for TGDP to participate in the program.

> "Always a wise question to ask, and I'm glad your team is thinking about it. I'll start by saying my primary goal is to ensure that your participation in the partner community adds value to your project and doesn't become a burden or a resource drain. So whatever we decide your committment looks like, we should couch it with that qualifier first.  
At the moment my vision is that we'll be able to collaborate with partners on something once every three or four months. So that means, e.g., we **check in every quarter** to see what you're working on, what stories we might tell, what you've achieved, what you'd like to announce, and so on. And then we collaborate on something that adds value to both our projects. I'm thinking this would take somewhere between **three and five person-hours**."

## The Vote

On May 4th, 2023 at 23:04 UTC in the #project-steering-committee Slack channel Bryan Klein proposed a Vote in favor of joining the GitLab Partners Program.

As of May 10, 2023 from the 12 members of the PSC Slack Channel, the votes are 12 in favor, 0 opposed. 
Reflected as such in the list below in alphabetical order.

| Vote | PSC Member                 |
|------|----------------------------|
| +1   | Aaron Peters (AP)          |
| +1   | Alyssa Rock (AR)           |
| +1   | Bryan Klein (BK)           |
| +1   | Cameron Shorter (CS)       |
| +1   | Carrie Crowe (CC)          |
| +1   | Deanna (D)                 |
| +1   | Erin McKean (EM)           |
| +1   | Felicity Brand (FB)        |
| +1   | Gayathri Krishnaswamy (GK) |
| +1   | Ryan Macklin (RM)          |
| +1   | Michael Park (MP)          |
| +1   | Tina Lüdtke (TL)           |

## Epilogue

May 10, 2023 UTC, BK sent [Doctopus Logo](https://gitlab.com/tgdp/brand-assets/-/blob/main/doctopus-mascot/Doctopus/SVG/BlueDoctopus.svg) to BB for use on the [partners page](https://about.gitlab.com/solutions/open-source/partners/).
BK also sent through TGDP email account, an email with key contact details to the ospartners@gitlab.com email address.
