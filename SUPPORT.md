# Support

For support with The Good Docs Project templates or other related resources, fill out our feedback form at:
https://www.thegooddocsproject.dev/feedback

We monitor responses to the feedback form and will respond or take action as necessary.

Alternatively, you can open an issue against any one of our repositories.

For the templates repository, open a new issue at:
https://gitlab.com/tgdp/templates/-/issues
