# DEI policy 

- [The Good Docs Project Diversity, Equity, and Inclusion Statement](#the-good-docs-project-diversity-equity-and-inclusion-statement)
  - [Purpose](#purpose)
- [Project access](#project-access)
- [Communication transparency](#communication-transparency)
- [Newcomer experiences](#newcomer-experiences)
- [Inclusive leadership](#inclusive-leadership)
- [Community health and safety](#community-health-and-safety)
- [Other efforts](#other-efforts)
- [Following this policy](#following-this-policy)
- [Feedback on this policy](#feedback-on-this-policy)


## The Good Docs Project Diversity, Equity, and Inclusion Statement


This statement defines the diversity, equity, and inclusion policy for The Good Docs Project. These guidelines apply to all members of the Good Docs Project and to all Good Docs Project activities, including but not limited to:

* The overall Good Docs Project community governance.
* All Good Docs Project working groups and meetings.
* All communications in our Discord server and community forums.
* All repositories under the `tgdp` organization in GitLab.

### Purpose 

At The Good Docs Project, we want to empower our community members to do their best work while being their authentic selves. We strive to promote greater diversity, equity, and inclusion in our project with concrete policies and actions as outlined in this document. We want to build a community where all people feel safe to participate, to introduce new ideas, and to inspire others. We welcome contributors from all backgrounds to our community.

Our project prioritizes and reflects on DEI through a regular review of project policies and performance. This reflection is documented in the following DEI.md file based on specific [CHAOSS project](https://chaoss.community) DEI metrics.

## Project access 

As an open source community, we strive to be open to all contributors. We want to lower the barrier to entry to our project by providing equal opportunities to contributors from all backgrounds and regions of the world. That means we commit to provide several access points and ways to contribute to the project.

Project access is addressed in our project through various efforts. Through these efforts, we aim to support access for all. Specific efforts in our project include:

* **The Good Docs project is an open source community in which anyone can join.** You don’t have to be a technical writer to join; you only need to care about documentation and want to educate and empower others to create high-quality documentation.  If you care about docs, you’re welcome!
* **We accept a wide variety of contribution types.** Community members can submit a wide variety of contributions, including DocOps, UX design and research, marketing and copywriting, software engineering, community management, project management, and more. We can use all types of contributions at our project. All areas of our project are open to community contributions.
* **We are a global community.** Our project maintainers, working group leads, and supporting teams span global time zones to provide support to contributors across the world. We offer multiple meeting times for all of our working groups, with meeting options for all regions: APAC, EMEA, and AMER. 
* **We actively maintain a public, community calendar.** We publicly publish our [community calendar](https://www.thegooddocsproject.dev/calendar) regularly and post Zoom links in our Discord server to encourage project members to attend. Anyone can join any meeting and all meetings are open by default.
* **We encourage working group members to consistently record meeting notes at all Good Docs Project meetings.** Consistently recording high-quality, thorough meeting notes supports our goals of maintaining transparency throughout the project and creating a welcoming environment. Our meeting notes make it easy for someone who couldn’t attend previous meetings to stay up-to-date on key discussion points, decisions, and action items.
* **We created an effective onboarding experience for community members.** Our Welcome Wagon is a meeting for newcomers to gather more information about the project. During the meeting, we provide an overview of The Good Docs Project’s mission, Code of Conduct,  goals and working groups. We also provide an opportunity to newcomers to tell us about their goals for contributing so that we can further guide them in connecting them with a working group lead,  joining a working group, and creating a successful contribution. 
* **We create and share training resources for community members.** We strive to provide training materials and resources for all members of the community regardless of their level of experience. These resources include a Git training course, a README and other how-to contribute guides for specific working groups. 
* **We ensure every major repository in our GitLab organization has community governance documents that are foundational to every healthy open source project.** That includes: a README, a Contributing Guide, an open source license file, our Code of Conduct, and additional documents as deemed necessary. For minor repositories, we will include stub versions of these documents that link to the general project version.

## Communication transparency 

We value communication transparency at The Good Docs Project because it builds trust within our community and makes our project better. When we are transparent, it helps community members feel safe and included. It shows that our project is open to considering new ideas and perspectives that could benefit the project. By keeping our documents, decisions, and communication channels open and public as much as possible, we ensure our community members have access to the context and information they need in order to make effective contributions and to have trust in the project leaders.

Communication transparency is addressed in our project through a variety of different efforts. Through these efforts, we aim to support transparency for all. Specific efforts in our project include:

* **We default to making things public instead of private.** If something is private, such as a document or Discord channel, we need to have a clear reason why that document or channel should not be made public to the community, such as the need to protect sensitive information or because of legal restrictions, or security requirements.
* **We regularly review our community governance documents to ensure they are accurate and up-to-date.** That includes: a README, a Contributing Guide, an open source license file, our Code of Conduct, and additional documents as deemed necessary. Repository owners are encouraged to regularly review and improve these documents to ensure they are up-to-date and accurate.
* **We consistently record meeting notes at all Good Docs Project meetings.** Meeting notes should be recorded in documents that are publicly accessible and that are owned by the project’s account rather than by any individual contributor. Working group meeting notes should be pinned in Discord or added to the description for that working group. If possible, working group leaders should post the meeting notes in our communication forums shortly after a meeting occurs to share with people who could not attend the meeting. The working group leads can also consider tagging absent community members on the post if they attend that meeting regularly.
* **We publicly distribute documents that contain major proposals and leave these documents open for community comment for at least a month.** Anyone in the contributing community can make a proposal. When someone makes a proposal, the project steering committee notifies stakeholders and shares the proposal across the community. Proposal authors should make a good faith effort to modify the proposal by applying community suggestions and feedback on the proposal. Once a decision has been made, we record that policy change or decision in accessible places. We also socialize the decisions across working groups.
* **We try to ensure that our project steering committee membership includes representatives from all the project’s working groups and supporting teams**. We want all community members to have a representative that can speak on their behalf, represent their concerns to project leadership, and vote on major project decisions. This ensures each group has a voice on the direction and governance of the project. If there’s something blocking or creating friction for those groups, those groups have an advocate within the project leadership.

## Newcomer experiences 

We care about creating positive newcomer experiences for our contributors. Creating a positive newcomer experience shows that we care about our contributors and want to set them up for success. It also shows that we’re considerate of our newcomers’ needs and want to support them as they get started with the project. Additionally, creating positive newcomer experiences helps to build future leaders within the project and benefits the project’s growth and longevity. 

The newcomer experience is addressed in our project through a variety of different efforts. Through these efforts, we aim to support the newcomer experience for all new members. Specific efforts in our project include:

* **We hold monthly Welcome Wagon meetings for newcomers to provide an in-depth introduction to our project.** Our welcome wagon meetings span three major geolocations (EMEA, APAC, AMER) to accommodate our global community of contributors.  In this meeting, newcomers learn about our project goals and mission, our working groups, and important community resources. They also get to meet other project members and leaders. 
* **We pair newcomers with a buddy in their working group so they have one-on-one support and a personal connection to the project.** These buddies can help answer questions and make sure that the new contributor has a solid understanding of the processes and best practices for working in the project.
* **We provide a Our Team page on our website that is kept up-to-date and provides helpful information for community members.** Our [Our Team](https://www.thegooddocsproject.dev/our-team) page tells community members who they can contact for support, especially if they are blocked on their project and need resources to continue.
* **We have a supportive Discord community.** Our Discord server has several channels where newcomers can go and ask for support and help.
* **We provide foundational documentation in all of our repositories.** All of our repositories have a contributing guide and a README that helps newcomers understand our contributing processes for each repository. These documents are reviewed every release cycle to ensure the content is fresh and accurate. 
* **We have a team of community managers who ensure that newcomers are welcomed and have access to onboarding resources as needed.** Our Tech Team is also available to help with requests to get added to issues, repositories, or other crucial documents.

## Inclusive leadership 

We want to make it easy for all project members to eventually become project leaders. Fostering inclusive leadership ensures our project can grow sustainably over time, prevents leadership burnout, and increases our project’s capacity to fulfill our mission. But more importantly, inclusive leadership ensures that we invite leaders from all backgrounds to share their unique strengths, talents, and perspectives with our project. Having diverse perspectives and talents prevents our project from stagnating by welcoming fresh ideas and approaches into the project.

Inclusive leadership is addressed in our project through a variety of different efforts. Through these efforts, we aim to support leadership opportunities for project members with an interest. Specific efforts in our project include:

* **We provide opportunities for contributors to move into middle leadership roles, such as serving as a working group lead or member of a supporting team.** Our teams are open to anyone who is interested and who has contributed to the project for at least one month. Opportunities to become a working group lead or supporting teams are available for all major geolocations. We provide a variety of opportunities for people with various interests and talents. As long as they are interested in participating, we will let them join and will mentor them in these roles. We also publicize how to become a member of these teams.
* **We actively work to promote contributors into middle leadership roles.** Working group leads regularly check in with contributors who have either made their first merge request or who have been a member of the project for a full release cycle to ask if they would be interested in moving into middle leadership.
* **We regularly invite contributors serving in middle leadership to join the project steering committee, which leads our project.** People who have been serving in middle leadership for a full release cycle or more are encouraged to join the project steering committee, which gives them a vote on project governance and direction.
* **We have project leaders who are dedicated to improving DEI.** This team seeks to have people from a variety of backgrounds and geolocations.
* **We acknowledge and thank project contributors at the end of every release cycle.** We encourage contributors and working group leads to call out contributors who made significant contributions or helped the project in key ways. This helps us reward and recognize everyone’s unique contributions and efforts and also identify potential future leaders in the project.
* **We strive to rotate leadership positions where possible.** If a community member has been serving in a role for an extended period of time, we encourage other members to step into those roles. For example, we encourage project steering committee chairs to abide by term limits.

## Community health and safety 

We believe in building a community that promotes psychological safety and where everyone feels they can be their authentic self. For that reason, we work to proactively set expectations about behavioral norms in our community. However, we recognize that conflicts may occur and community members may possibly violate our community guidelines. We also recognize that community members with less power or privilege are especially vulnerable to potential harm. Harmful behavior and interpersonal conflicts in the community carry the risk of escalating, lowering morale, and preventing our community from working on its core mission. To prevent or mitigate these situations, we believe in having clear processes for preventing harm and handling community conflicts so that we can keep our community productive and cohesive. 

Specific efforts in our project include: 

* **We work to prevent burnout in our community.** We encourage all community members to prioritize work-life balance. We make accommodations for community members who need to step back from the project temporarily or permanently to focus on their family, work, or other personal matters. We encourage taking breaks by building in 3-week rest periods into our release cycles.
* **We work to prevent burnout in project leadership.** To help prevent burnout, our leadership teams make an effort to check on everyone’s overall well-being and stress levels and take action if needed. We build redundancy into our leadership models by having more than one lead for working groups or leadership teams, where possible.
* **We have a Code of Conduct that is publicly posted in multiple places and which all new members are required to agree to uphold before participating in our community.** To ensure all members are aware of our community guidelines, we do not allow access to our contributing community or community forums until a new contributor has acknowledged and agreed to our Code of Conduct. We also share the Code of Conduct at Welcome Wagon meetings for new contributors, typically as one of the first agenda items. At these meetings, we also carefully explain how people can report a Code of Conduct incident to our community management team.
* **Our Code of Conduct applies equally to all members, including project leaders.** The purpose of our Code of Conduct is to protect members of our community from harm in community spaces. Contributors who have the least amount of power and privilege can benefit the most from a Code of Conduct. As people of privilege within the project, we expect our project leaders to follow the Code of Conduct as well. Project leaders will not be given special treatment if they violate community norms.
* **We have a clear, well-defined method for handling Code of Conduct incidents.** At our project, we believe that a Code of Conduct is only valuable if it is accompanied by a straightforward and transparent process for handling Code of Conduct incidents. We have made an effort to think through the logistical details of how we will handle our Code of Conduct incidents in an efficient, fair, and consistent way that ensures the privacy and safety of incident reporters. We ensure the method for submitting a Code of Conduct report is clear and easy. We also provide our Code of Conduct incident process in a publicly available document. We also have a process for recusing incident handlers who may have a conflict of interest.
* **We ensure that our community management team is properly trained in handling Code of Conduct incidents.** We recognize that effectively handling Code of Conduct incidents requires adequate training and knowledge. To ensure our community management team is trained and prepared, our team meets regularly and reviews various aspects of our Code of Conduct and our incident handling process in each team meeting. Where possible, our team simulates and role-plays Code of Conduct incidents to ensure they are prepared to handle an actual incident.
* **We work to prevent burnout in our community management team.** We recognize that resolving conflicts and Code of Conduct incidents has the potential to burn out the team members that are responsible for handling incidents. To help prevent burnout, our community management team makes a special effort each meeting to check on everyone’s overall well-being and stress levels and take action if needed. We also regularly seek to invite new community managers in to refresh and reinvigorate the team as needed.

## Other efforts 

At the Good Docs Project, we try to go above and beyond to make our project inclusive and welcoming to contributors from all backgrounds. We want to eliminate or address any barriers to contributing, including time commitments and the need for volunteer-life balance. In addition to the efforts explained in the previous sections of this document, we believe that we should have additional efforts to foster diversity, equity, and inclusion.

Additional specific efforts in our project include: 

* **We mentor and/or formally train our community members about the core technologies needed to contribute to our project, such as Git, GitLab, and Markdown.** Our community management team offers Git training workshops to members who have shown a commitment to the project and who are in need of additional skills in order to be able to contribute. These workshops lower the technical barrier to entry in our project and equalize the playing field so that anyone in the project can contribute.
* **We allow for flexible contributing time commitments**. We tell all community members that: “We’ll take whatever you can give. You and your family come first, then work, then comes volunteering for The Good Docs Project. If one of those pieces in your life is taking up more bandwidth than normal, such as maybe a health problem or family situation or an intense work project, that’s okay.”
* **All project deadlines are soft deadlines.** To reduce stress and burnout, our releases are  timed releases, not scoped releases. That means that our releases represent the work that was done in that period of time rather than being released when a set of features is ready. Having timed releases allows our project to treat release deadlines as goals rather than due dates. Our deadlines act as a motivation to wrap up work. They also help us to track our project as we get closer to the release deadline.
* **We value internal documentation.** So-called “tribal knowledge” is antithetical to inclusivity because the knowledge of how to effectively contribute is trapped in the minds of the current project leaders and contributors. To combat this anti-pattern, we work to ensure that all working groups clearly document their processes and crucial knowledge in either an internal knowledge base, on GitLab, or on our public websites.
* **We build in regular moments to review our policies and reflect on how we can do better.** At the end of every release cycle, we encourage all working group meetings to hold regular retrospectives where all the project members are invited to share their honest feedback about where we could improve and brainstorm solutions for improvement together.  We also build in regularly scheduled times to assess and review important policies such as our DEI policy and our Code of Conduct to determine which efforts are working or where we could improve.

## Following this policy 

At the Good Docs Project, we are committed to living by our DEI policy. In order to ensure that we follow this policy, our community management team will review this policy once a release cycle to assess:

* How well we are adhering to this policy.
* Whether this policy needs revisions in order to better serve our community’s needs.
* Inviting and encouraging feedback on this policy from the project steering committee and the larger community.

This policy is aspirational and there may be times when we do not always uphold these policies perfectly. Our ability to meet some of these goals might fluctuate depending on resources and member availability. However, we are committed to regularly reviewing the policy and evaluating how we can try to meet these ideals.

## Feedback on this policy 

Our project recognizes that the inclusion of the DEI.md file and the provided reflection on the specific DEI metrics does not ensure community safety nor community inclusiveness. The inclusion of the DEI.md file signals that we, as a project, are committed to centering DEI in our project and regularly reviewing and reflecting on our project DEI practices.

If you do not feel that the DEI.md file appropriately addresses concerns you have about community safety and inclusiveness, please let us know. You can do this by reporting your concerns to our [community management team](https://thegooddocsproject.dev/who-we-are/#community-managers).


Last reviewed: June 2024

