# Code of conduct response plan - The Good Docs Project

`Version 1.0 - Last updated March 2023`

This document explains:

- How to contact the current Good Docs Project community managers (such as to report a [Code of Conduct](CODE_OF_CONDUCT.md) incident).
- The policies and procedures community managers should follow when responding to an incident.
- Additional governing policies for the community management team (also sometimes referred to as the "Code of Conduct committee").

## Community managers

The following individuals are current Good Docs Project community managers and are available on the Good Docs Slack workspace at these handles:

- Alyssa Rock - @alyssarock
- Carrie Crowe - @carrie crowe
- Deanna Thompson - @deanna

You may contact any of these individuals to make a Code of Conduct incident report.

## Contacting a community manager

You can contact a community manager to make a code of conduct incident report or to discuss the process and options related to code of conduct incidents.
To make an incident report, send a message to the community manager that has the best working relationship with you and with whom you feel most comfortable talking.

To contact a manager, send them a direct message (in other words: a private, written message) on the Good Docs Project Slack.
Community managers will respond as soon as they can.
They might also request a one-on-one meeting with you (such as a phone call or online video conference) to get more information about the incident.

## Community manager values

Community managers should strive to:

- Occupy a position of trust and good rapport within the Good Docs Project community.
- Be active listeners who can show empathy and understanding when meeting with an incident responder.
- Respect the privacy of incident reporters or other potentially sensitive and private information they may have access to in this role.
- Be fair and open-minded when investigating an incident and recommending a response.
- Develop healthy self-care strategies to prevent burnout and reduce personal stress.

## Requirements for community managers

The Good Docs Project community managers play an important role in the community because they help ensure the community is healthy, vibrant, and welcoming to all contributors.
Because of the crucial nature of this role, potential community managers should be invested in the long-term health of the Good Docs Project community and should be willing to develop a set of communication skills that may require some formal training.
For that reason, individuals who are interested in serving as community managers must:

- Commit to formal training in incident response skills within a month of joining the community management team. Formal training involves reading a Code of Conduct response book or taking an online training course in incident response skills, mediation, or arbitration. Or it could also involve participating in an informal workshop in incident response skills led by a current Good Docs Project community manager. Upon joining the community management team, the new manager can work with other managers to develop a training plan and ensure this training requirement is met.
- Be recommended for the team by either another community manager or a member of the Good Docs Project project steering committee. Recommended individuals should demonstrate the values and qualities necessary to carry out the responsibilities of community managers. Note that Good Docs Project community members may first volunteer for consideration and then seek a recommendation afterwards.
- Be active contributors to the Good Docs Project who have participated in the community for at least three months. Contributions can include authoring pull requests, submitting issues, participating in or leading a working group, attending meetings regularly, and participating in Good Docs Project community forums.
- Not be subject of an ongoing Good Docs Project code of conduct incident response.

## Community manager terms of service

The Good Docs Project community manager team should consist of 3-5 community members at a given time.
To prevent burnout, community manager should serve for a recommended limit of 6-12 months at a time.
Community members should stagger terms of service to ensure there is some continuity on the team over time.
Although community managers may serve additional terms of service, they should take a break from service from time to time, especially if they've lead investigations into multiple code of conduct incidents.
If possible, outgoing community managers should recommend a replacement from the community.

## Reviewing the Code of Conduct

The code of conduct and this document (the Code of Conduct process document) should be reviewed by the community management team at least once a year, typically in April, to ensure these documents are meeting the needs of the community.
The Good Docs Project community will notify the community of any revisions by publicizing the revisions in the community's forums of communication on the community's forums.


## Key terms used in this document

This section provides a definition of key terms and roles that appear in the incident response policy that follows this section:

- **Incident** - A behavior by a member of the the Good Docs Project that allegedly violates the community [Code of Conduct](CODE_OF_CONDUCT.md). Also known as a "Code of Conduct incident" or "conduct violation."
- **Incident report** - Begins when a member of the the Good Docs Project reports behavior that violates the community Code of Conduct. The incident report refers to the violating behavior that is then investigated by community moderators. Also sometimes referred to as the "report."
- **Incident reporter** - The person who reports a Code of Conduct violation to a community manager. Also sometimes referred to as the "reporter."
- **Handling an incident report** - The process of investigating and resolving an incident report as explained using the processes and guidelines in the subsequent sections. Also known as "investigating a report."
- **Investigating manager** - The community manager who will handle the incident report and ensure the report moves through all six stages. Also known as the "investigator."
- **Accused individual** - The accused individual is the person who is alleged to have violated the Code of Conduct.
- **Escrowed reports** - An incident report where the reporter has not give permission to proceed with an investigation. If the reporter gives permission to keep the report "in escrow," these escrowed reports will not be acted upon until there is a second report of the same incident or a similar incident involving the same individual. The goal of an escrow report is to retain a record of incidents in case there is a pattern of misbehavior by the same individual.


## Handling incident reports

An incident report begins when a member of the Good Docs Project contacts a community manager to report an incident.
The community manager who is contacted should handle the incident report and should try to respond as soon as possible.
This community manager will become the investigating manager.

The investigating manager may involve another community manager as an additional supporting investigator or as a replacement investigator under these conditions:

- If the community manager who was contacted by the incident reporter does not feel comfortable investigating and handling the incident alone.
- If the investigating manager cannot handle the incident in a timely manner and must ask a different community manager to investigate the incident report.
- If the community manager needs to be recused because of a conflict of interest.

If the community manager who was contacted by an incident reporter intends to involve an additional community manager for support or as a replacement, they should first inform the incident reporter, explain the circumstances, and offer the
opportunity to withdraw their incident report if they are uncomfortable having another community manager involved.

To promote impartiality, if the incident reporter is a community manager themselves, then a different community manager must handle the report.
See <a href="#conflicts-of-interest">Preventing conflicts of interest</a> for more information.

### Overview

All incident reports have six stages:

1. Listen
2. Triage
3. Recommend
4. Respond
5. Follow up
6. Resolve

See the following sections for more information about what occurs in each phase.

### Listen

During the listening phase, the investigating moderator will:

- Listen to the incident reporter’s explanation of the Code of Conduct violation.
- Explain the available outcomes.
- Obtain permission to proceed to the next steps in the investigation.
- Fill out the [Code of Conduct incident record](code-of-conduct-incident-record.md). NOTE: This record can be filled out after taking the report if needed.

Throughout the process, the investigating moderator will treat the reporter’s identity as confidential and will only disclose their identity to other moderators on a need-to-know basis.

The investigating moderator should talk directly to the person who reported the incident either through an online video conference or by phone.

During or immediately after this meeting, the investigating manager should:

- Note the reporter's name and contact information.
- If possible, note the incident's date, time, and/or location.
- Listen carefully to the incident reporter and get a complete understanding of the incident, its context, and the parties involved. The investigating manager should strive to listen with empathy and understanding. **They should default to believing the incident responder.**
- Ask what the incident reporter would need in order to feel emotionally whole or restored. Explain the possible outcomes that are available, as provided in the [Code of Conduct](CODE_OF_CONDUCT.md) (correction, warning, temporary ban, permanent ban). However, the investigating manager should not make any direct promises for exactly how the report will be handled until the investigation is concluded.
- Obtain permission from the incident reporter to proceed with the investigation. If permission is not granted, the investigator can offer to hold the incident report in escrow. Escrowed reports will not be acted upon until there is a second report of the same incident or a similar incident involving the same individual. The goal of escrow reports is to retain incident reports in case there is a pattern of misbehavior by the same individual.

During or immediately after the meeting, the investigating moderator should:

- Fill out the [Code of Conduct incident record](code-of-conduct-incident-record.md) to ensure that all information from the meeting has been accurately captured. The investigating manager should avoid over-documenting the incident: only document information required to inform the report resolution. Where possible, avoid documenting your opinion about the incident, or any information about individuals that is not relevant to the report.
- File the incident record in the project's Google Drive. See the Tech team for assistance if needed.
- If permission was not obtained to proceed with the report, the incident report is kept in the incident record archives. If the incident reporter wanted to keep the report in escrow, the incident report is kept in the escrow incident report archives.
- If permission was obtained, proceed with the rest of the investigation.

If necessary, the community manager may need to conduct additional interviews with other corroborating witnesses or may have to review any additional recorded evidence of the incident (such as emails, documents, message transcripts, or chat histories).


### Triage
After completing the listening phase, the manager should assign an initial risk and impact level to the incident using their best judgment based on the following guidelines.

#### Severity levels
Severity refers to the overall seriousness of the behavior and the risk that behavior will be repeated:

<table>
  <tr>
    <th>Severity level</th>
    <th>Definition</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td>High</td>
    <td>
      <ul>
        <li>The incident is extremely severe and/or there is a high likelihood the behavior will occur again in the future.</li>
        <li>Incidents that are harassing, dangerous, abusive, violent, offensive (especially to marginalized groups), or which threaten the physical and/or psychological safety of one or more community members are designated as high severity.</li>
        <li>Repeated medium- or low-level offenses by the same individual are also automatically designated as high severity.</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Sexual assault or unwanted sexual attention</li>
        <li>Violent threats or language</li>
        <li>Personal attacks</li>
        <li>Derogatory language (especially aimed at marginalized groups)</li>
        <li>Repeated inappropriate comments after a warning</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Medium</td>
    <td>
      <ul>
        <li>The incident is moderately severe and is potentially disruptive to the community.</li>
        <li>The incident could possibly cause one or more community members to feel unwelcome or uncomfortable in the community.</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Mildly inappropriate comments or jokes</li>
        <li>Bullying</li>
        <li>Tone-policing</li>
        <li>Repeatedly dominating a conversation (such as repeatedly talking over another person or not inviting discussion from others where appropriate)</li>
        <li>Excessive profanity</li>
        <li>Sustained disruptions of community events</li>
    </td>
  </tr>
  <tr>
    <td>Low</td>
    <td>The incident is minor in nature and doesn't pose serious harm or risk of harm.</td>
    <td>
      <ul>
        <li>Heated discussions or disagreements between community members.</li>
      </ul>
    </td>
  </tr>
</table>


#### Impact levels
Impact refers to how public the incident was and the number of community members who were or who could have been impacted by the incident, especially members of marginalized communities:

<table>
  <tr>
    <th>Impact level</th>
    <th>Definition</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td>High</td>
    <td>
      <ul>
        <li>The incident occurred in a public event, in a Good Docs Project meeting or community event, or on a community forum (such as on the mailing list or in chat, such as on the Slack channel).</li>
        <li>The accused individual is a Good Docs Project leader or a high-profile community member.</li>
        <li>Incidents involving someone who was representing the Good Docs Project in an official capacity while the incident occurred.</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Comments in the Good Docs Slack or mailing list.</li>
        <li>Comments or actions in a Good Docs Project meeting.</li>
        <li>Speaking or participating at a conference or fund-raising event as a representative of the Good Docs Project.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Low</td>
    <td>
      <ul>
        <li>The incident occurred in a private conversation, message, or email. Also includes posts or comments made in a forum or context outside of official Good Docs Project, such as on a personal social media account.</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>Comments in a private email.</li>
        <li>Comments in a direct message on Slack.</li>
        <li>Comments or actions made in a one-on-one meeting in person or virtually.</li>
      </ul>
    </td>
  </tr>
</table>

### Recommend

Once an initial risk or impact level has been assigned, the investigating manager should send a private message to the rest of the community managers through email.
Community managers who have recused themselves over conflicts of interest should not be included in this email.
A separate direct message on Slack to notify the other managers to check for the email would also be appropriate to ensure everyone is aware of the email.

In the email, indicate your assessment of the incident's severity and impact level and your recommended response.
See the [Code of Conduct](CODE_OF_CONDUCT.md) for the four possible responses to a conduct violation (correction, warning,
temporary ban, permanent ban).

Community managers should respond as soon as possible whether they concur with the incident severity and impact levels and the recommended response.
If community managers disagree with the original assessment, they should indicate the nature of their disagreement.
Where disagreements occur, the committee should work quickly to reach a consensus (ideally within 1-2 days) and may require a video conference discussion.
If a consensus cannot be reached and has ended in a stalemate, the response should be put to a vote.
In incidents where a tied vote occurs, the chair of the community managers acts as the deciding vote.

After a response has been recommended, the incident reporter should be notified of the outcome of the investigation and the recommended response before proceeding.

### Respond

Once the incident response has been determined by the community managers, the investigating manager should meet with the accused individual in person (either through an online video conference or by phone).
The investigating manager may invite an additional community manager to attend the meeting if support is desired.

Before this meeting, the investigating moderator should fill out the [Code of Conduct remediation record](code-of-conduct-remediation-record.md) and use this document to guide the meeting.

In this meeting, the community manager should explain the nature of the reported incident and the specifics of the incident response (correction, warning, temporary ban, permanent ban).
The accused individual will be given a chance to respond (within reason) and will be informed about the process for appealing the incident response.

If a new code of conduct violation occurs in this meeting (such as a derogatory or threatening comment made to a community manager or about another member of the community), it should be treated as a separate incident and should be reported as a new incident to the community manager.

To protect the identity of the incident reporter, the accused individual must not be given the identity of the incident reporter nor will they be allowed to contact the incident reporter, even to apologize.
If an apology is required as part of the response, the following options are permissible:

- The apology can be delivered to the investigating community manager who will then deliver it to the incident reporter.
- The apology may be delivered in a public forum.

During or immediately after the meeting, the investigating manager should fill out the any additional notes on the Code of Conduct remediation record to ensure that all information from the meeting has been accurately captured.

### Resolve

The investigating manager should implement the consequence(s) of the incident response, depending on what the response was.
The manager should also follow up with the incident reporter to let them know what the outcome of the report was.

If a temporary ban was implemented, the community manager who handled the incident should meet with the accused individual to ensure compliance before readmittance into the community.

All documentation should be moved to the appropriate project Google Drive folder:

- The [Code of Conduct incident record](code-of-conduct-incident-record.md) form.
- The [Code of Conduct remediation record](code-of-conduct-remediation-record.md) form.

It is important to file this documentation to enable the community managers to identify and prevent potential repeated patterns of abuse.


## Handling incident appeals

If an accused individual wants to dispute the decision of the community managers, that individual is entitled to one appeal.
An appeal can be requested directly from the community managers using the same process of reporting an incident.
That means that the accused individual can send a direct message to one of the community managers to request an appeal. While the appeal process is underway, the accused individual must still comply with the incident response plan.

When an appeal is requested, 2-3 members of the Good Docs Project steering committee will review the incident documentation and the reason for the appeal.
They will consult with the community managers about the investigation and decision-making process to determine if the code of conduct was fairly and properly applied.
They will then recommend to uphold, modify, or reverse the original incident response.
Decisions made by the project steering committee are final.

<a name="conflicts-of-interest"></a>
## Preventing conflicts of interest

A community manager is considered to have a conflict of interest when any of the following conditions are met:

- The community manager is the individual accused of a code of conduct violation.
- The community manager has a close working or personal relationship with the individual accused of a code of conduct violation that could impede their ability to be impartial.
- The community manager was personally involved in the code of conduct violation in some way (such as being the direct target of a code of conduct violation). Merely witnessing or being present during the incident does not necessarily qualify as a conflict of interest. Merely being part of a protected group that was targeted in a derogatory statement or action does not necessarily qualify as a conflict of interest.

Community managers that meet any of these conditions should recuse themselves from all discussions and decisions about the incident where they have a conflict of interest.
Another member of the community management team should act as the investigating manager.
The community manager with a conflict of interest should ensure that another community manager is designated to handle the incident.

If the accused individual is a leader or prominent member of the Good Docs Project community, avoidance of a conflict of interest may not be possible as all community managers could possibly have a personal working relationship with the accused individual.
In this situation, recusal is not necessary and managers should instead make their best effort to remain impartial.
