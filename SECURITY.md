# Security reporting

The Good Docs Project templates and resources are for documentation purposes. Since our files are typically in Markdown or other open formats, our open source project currently has no need for security reporting.
