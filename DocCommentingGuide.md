# Commenting guide for collaborative document reviews

*Tips for applying comments and track-changes during collaborative
document reviews, when using tools like Google Docs, Word, LibreOffice
and Pages. Many tips also apply to docs-as-code reviews in tools like
GitHub and GitLab.*

**Verson:** 1.1

**Date:** July 2024

## Scope

Tips in this guide are considered recommendations, to be applied most of
the time, rather than being hard rules.

## Tips for reviewers

### Comments instead of track-changes

Use comments instead of track changes.

  - Documents become messy and difficult to understand when multiple
    reviewers apply track changes on top of each other.
  - A later reviewer should be able to easily understand the author’s
    original intent.

#### Possible exceptions

  - Trivial changes, such as punctuation tweaks.
  - Adding all the text for a new section.
  - If there is only one reviewer.

### Highlight one word instead of a slab of text

Select a word or a few words instead of a sentence or a slab of text to
comment on.

  - This ensures space is left for other reviewers to attach a separate
    comment.

### One comment thread per idea

Don’t put multiple ideas into one comment.

  - This allows a thread to develop around one idea, and for each idea
    to be closed separately.

### Add to comments

  - You can expand on, or disagree with, a prior reviewer’s idea by
    replying to their comment.
  - You can support a comment by simply replying with a “+1”.

### Adopt commenting conventions

Adopt a commenting convention, such as
<https://conventionalcomments.org/>.

  - This helps with consistency and conveying the importance of each
    comment.

Commonly used <https://conventionalcomments.org/> labels include:

  - **suggestion:** Propose improvements to the current subject.
  - **nitpick:** Trivial preference-based request.
  - **praise:** Highlight something positive.
  - **question:** You have a potential concern but are not sure if it's
    relevant.
  - **thought:** An idea that popped up while reviewing.

Sometimes add a decoration:

  - **(blocking):** Should block acceptance until resolved.

Example:

  - *“**suggestion (blocking):** This sentence is ambiguous. Suggest
    reword as {…}”*

### Reference best-practices

If your comment recommends applying a principle from a guide or best
practice, provide a hyper-link to the relevant section in the guide.

  - This helps the author learn about best practices.
  - Referencing best practices provides justification and authority to
    support your reasoning.

Typically only provide one reference to a best practice, in the first
applicable comment.

#### Possible exceptions

  - As a reviewer, you may be time-constrained. Finding references is
    time-consuming.
  - You may know that the author is already familiar with the relevant
    section of the guide.

### Login before commenting

Preferably login to your Google or Microsoft account before commenting
on a doc.

  - Comments are attributed to you rather than “Anonymous”, and adds a
    personal touch to your community.
  - You will receive email updates when an author responds to, or
    resolves, your comment.

## Tips for authors

### Set a review timeframe

An author should specify a timeframe for feedback when asking for a
review.

  - This helps stakeholders plan their time.

### Select appropriate reviewers and scope

  - Typically, different reviewers would be asked to consider different
    review criteria based on their skillset.
  - Ensure the depth of review requested aligns with the document
    importance.

An author should describe scope and criteria when asking for a review.
Criteria may include:

  - Technical accuracy.
  - Clarity and related writing principles.
  - Alignment with a template.
  - Alignment with a style guide.

### Designated authority to accept/reject changes

A designated person makes the final decision to accept or reject all
non-trivial comments.

  - Typically, this is the document author, but may be a senior reviewer
    or subject matter expert.
  - Successful incorporation of advice is a key indicator of quality
    documentation.

### Reasoned reply to each comment

An author should reply to each non-trivial comment before closing.
Typically note your action and reasoning. Feel free to add a personal
touch to your message.

Examples:

  - “Agree, done.”
  - “Thanks for the suggestion, done.”
  - “Not done, {short reason}.”

Why reply:

  - Replying is a courtesy to the reviewer.
  - It helps the reviewer understand the reasoning behind the decision.
  - Sometimes it provides a learning opportunity for the reviewer.
  - It provides an opportunity for a reviewer to clarify a misunderstood
    comment.

In Google Docs and Word this feedback is automatically emailed to the
commenter.

### Resolve completed comments

Once actioned, an author should select a comment thread’s “Resolved”
button to hide the comment thread.

  - This helps the author and readers see what’s left to be actioned.

### Use version history - name releases

Label each document version you put out for review (if your tool
supports it).

  - This enables reviewers to compare changes between reviews, which
    reduces their review overhead.

In Google Docs, this is supported under File -\> Version History. (It
requires reviewers to have Edit Access to the doc, which enables viewers
to see Version History.)
