# Announcements

(Archived from old wiki. Last edited by Derek Ardolf, "2 years ago")

* 2020-08-09 Notes and resource links from [The Good Docs Project table during Writing Day at Write The Docs 2020 - Portland](https://dev.to/scriptautomate/writethedocs-portland-2020-writing-day-debrief-notes-3pf2)
* 2020-05-27 Article about Felicity, including [her involvement in TheGoodDocsProject](https://typo3.org/article/typo3-book-report-whos-writing-the-typo3-book)
* 2020-03-12 Insights from involvement in [Season of Docs 2019](http://cameronshorter.blogspot.com/2020/03/insights-from-mixing-writers-with-open.html).
* 2019-12-16 Announcing TheGoodDocsProject at the Developer Relations Conference in London. Jo Cook's [blog post](https://archaeogeek.com/blog/2019/12/15/devrelcon2019/) and [slides](https://github.com/archaeogeek/devrelcon2019).
* 2019-11-25 Launching TheGoodDocsProject at WriteTheDocs - Australia conference (and conference highlights) [Cameron Shorter's blog post](http://cameronshorter.blogspot.com/2019/11/launching-thegooddocsproject.html).
* 2019-11-11 [[Announcement-1]] Fixit Workshop, at WriteTheDocs - Australia conference.
